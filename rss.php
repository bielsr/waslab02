<?php
require_once("DbHandler.php");

setlocale(LC_TIME, "en_US");
$host = $_SERVER['HTTP_HOST'];
$base_url = 'http://' . $host;
$dbhandler = new DbHandler();

$rss = new SimpleXMLElement("<rss></rss>");
$rss->addAttribute('version', '2.0');
$resp = $rss->addChild('channel');

$resp->addChild('title', 'Wall Of Tweets');
$resp->addChild('link', $base_url);
$resp->addChild('description', 'RSS 2.0 feed that retrieves the tweets posted to the web apps "Wall of tweets 2"');
$res = $dbhandler->getTweets();
foreach($res as $tweet) {
    $item = $resp->addChild('item');
    $item->title = $tweet['text'];
    $item->link = $base_url . '/wall.php#item_' . $tweet['id'];
    $item->description = 'This is WoT tweet #' . $tweet['id'] . ' posted by <b>' . $tweet['author'] . '</b> It has been liked by ' . $tweet['likes'] . ' people';
}

header('Content-type: text/xml');
echo $rss->asXML();

?>
