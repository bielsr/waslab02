<?php
$URI = 'http://localhost/wall.php';

$postdata = <<<XML
<?xml version="1.0"?>
<tweet>
    <author>Test Author</author>
    <text>Test Text</text>
</tweet>
XML;


$opts = array('http' =>
    array(
        'method' => 'PUT',
        'header' => 'Content-type: text/xml',
        'content' => $postdata
    )
);
$context = stream_context_create($opts);
$result = file_get_contents($URI, false, $context);
echo $result;
echo $sx = new SimpleXMLElement($result);
$created_id = $sx->tweetid;
$URI_DELETE = $URI . "?twid=" . $created_id;

$opts = array('http' =>
    array(
        'method' => 'DELETE',
        'header' => 'Content-type: text/xml',
    )
);
$context = stream_context_create($opts);
$result_del = file_get_contents($URI_DELETE, false, $context);
echo $result_del;


$resp = file_get_contents($URI);
echo $http_response_header[0], "\n"; // Print the first HTTP response header
$sx = new SimpleXMLElement($resp);
$tweets = $sx->children();
foreach ($tweets as $tweet) {
    echo "[tweet #" . $tweet['id'] . "] " . $tweet->author . ": " . $tweet->text . " [" . $tweet->time . "]\n";
}


?>